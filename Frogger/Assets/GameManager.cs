﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [SerializeField] GameObject _menu;
    [SerializeField] GameObject _gameUI;
    [SerializeField] GameObject _map;
    [SerializeField] GameObject _popUp;
    [SerializeField] GameObject _tappingArea;
    [SerializeField] GameObject _tutorialSection;

    [SerializeField] LevelList _levelList;
    public LevelList LevelList { get { return _levelList; } }

    private void Start()
    {
        Instance = this;
        _menu.SetActive(true);
        _gameUI.SetActive(false);
        _popUp.SetActive(false);
        Time.timeScale = 1;
    }

    public void LoadLevel()
    {
        _menu.SetActive(false);
        _gameUI.SetActive(true);
        if (PlayerPrefs.GetInt("IsFirstGame", 0) == 0)
        {
            Debug.LogError("2");
            _tutorialSection.SetActive(true);
            _tappingArea.SetActive(false);
        }
        //activate splash screen and also maybe run script inside to wait for elements on map 
        //on map start run loading
        _map.SetActive(true);
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            NextLevel();
        }
        else
        {
            SplashScreen.Instance.StartFading();
        }
        //run something like run screen?  
    }
    public void OpenLevelsMenu()
    {
        _menu.SetActive(false);
        // need to have an instance because need to load level 
        if (!MapController.Instance || !_map.activeSelf)
        {
            _map.SetActive(true);
        }
        LevelsMenu.Instance.ShowMenu();
    }
    public void Win(int score)
    {
        _tappingArea.SetActive(false);
        PlayerPrefs.SetInt("CurrentLvl", PlayerPrefs.GetInt("CurrentLvl", 0) + 1);
        Time.timeScale = 0f;
        _popUp.SetActive(true);
        SetPopUp.Instance.SetValue(true);
    }
    public void NextLevel(int lvl = default(int))
    {
        _popUp.SetActive(false);
        MapController.Instance.NextLevel(lvl);
        if (!_gameUI.activeSelf)
        {
            _gameUI.SetActive(true);
        }
        ResetLevel();
    }
    public void Loose()
    {
        _tappingArea.SetActive(false);
        Time.timeScale = 0f;
        _popUp.SetActive(true);
        SetPopUp.Instance.SetValue(false);
        //and set values
    }
    public void ResetPlayer()
    {
        PlayerManager.Instance.ResetPlayer();
        InGameUI.Instance.SetTime();
    }
    public void ResetLevel()
    {
        SplashScreen.Instance.StartFading();
        PlayerManager.Instance.ResetPlayer();
        InGameUI.Instance.SetLevel();
        Time.timeScale = 1f;
        _popUp.SetActive(false);
    }
  
    public void OpenMenu()
    {
        _menu.SetActive(true);
        _gameUI.SetActive(false);
        _popUp.SetActive(false);
        _tappingArea.SetActive(false);
    }
}
