﻿using UnityEngine;

public class PlayerCollisionDetecting : MonoBehaviour
{
    #region PrivateVars
    int _panelsCount;
    #endregion

    #region PrivateMethods
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "WinBox")
        {
            InGameUI.Instance.CompleteRun();
        }
        if(collision.gameObject.tag == "RiverBox"|| collision.gameObject.tag == "CarBox")
        {
            InGameUI.Instance.Die();
        }
        if(collision.gameObject.tag == "WoodBox")
        {
            PlayerManager.Instance.IsGrounded = true;
            _panelsCount++;
            InGameUI.Instance.PanelsCount = _panelsCount;
        }
        if (collision.gameObject.tag == "StreetBox")
        {
            PlayerManager.Instance.IsGrounded = true;
            _panelsCount++;
            InGameUI.Instance.PanelsCount = _panelsCount;
        }
        if (collision.gameObject.tag == "StartBox" || collision.gameObject.tag == "GrassBox")
        {
            PlayerManager.Instance.IsGrounded = true;
        }
    }
    #endregion
}
