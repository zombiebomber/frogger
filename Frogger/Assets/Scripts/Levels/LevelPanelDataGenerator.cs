﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
/// <summary>
/// Data generator for levels. Create List with levels, levels with panel list, panel with randoms value
/// </summary>
public class LevelPanelDataGenerator : ScriptableObject
{
#if UNITY_EDITOR
    //generate  level list, level panels, panels
    public void DataGenerator(int LvlCount, int difficulty)
    {
        // if LevelsList already exist just take it \.o_o./
        bool _levelsExist = true;
        LevelList _lvlList;
        _lvlList = (LevelList)AssetDatabase.LoadAssetAtPath("Assets/Resources/Levels/Levels.asset", typeof(LevelList));

        if (_lvlList == null)
        {
            _lvlList = ScriptableObject.CreateInstance<LevelList>();
            _levelsExist = false;
        }

        //Set folder name by difficulty
        string dif;
        if (difficulty == 0)
        {
            dif = "Easy";
        }
        else if (difficulty == 1)
        {
            dif = "Medium";
        }
        else
        {
            dif = "Hard";
        }

        //if difficulty folder not exist create it
        if (!Directory.Exists("Assets/Resources/Levels/LevelList" + dif))
        {
            Directory.CreateDirectory("Assets/Resources/Levels/LevelList" + dif);
        }

        //create much levels as we wants
        //variable to hold levels count in actual difficuly directory
        int directoryLevels = 0;

        for (int lvls = 0; lvls < LvlCount; lvls++)
        {
            //Create level asset
            LevelPanelsList _newLevel = ScriptableObject.CreateInstance<LevelPanelsList>();

            // get levels count from this difficulty for new numeration
            if (Directory.Exists("Assets/Resources/Levels/LevelList" + dif + "/Level" + lvls) && lvls == 0)
            {
                DirectoryInfo dirInfo = new DirectoryInfo("Assets/Resources/Levels/LevelList" + dif);
                directoryLevels = dirInfo.GetDirectories().Length;
            }

            //random panels count
            int linesCount;
            linesCount = Random.Range(2, 2 + (int)(difficulty) + lvls);

            //CREATE PANELS
            for (int panel = 0; panel <= linesCount; panel++)
            {
                //CREATE NEW PANEL
                LevelPanel _newPanel = ScriptableObject.CreateInstance<LevelPanel>();
                if (!Directory.Exists("Assets/Resource/Levels/LevelList" + dif + "/Level" +(directoryLevels+ lvls)))
                {
                    Directory.CreateDirectory("Assets/Resources/Levels/LevelList" + dif + "/Level" + (directoryLevels + lvls));
                }
                
                //set panel speed
                _newPanel.speed = 0.001F * (Random.Range(4, (int)(difficulty) + 7));
                
                //random elements size
                int size = Random.Range(3, 3 + (int)(difficulty) * panel);

                //Panels Types half = cars, half = woods
                if (panel <= linesCount / 2)
                {
                    _newPanel.type = true;
                }
                else
                {
                    _newPanel.type = false;
                }

                //set panel element sizes
                for (int i = 0; i < size; i++)
                {
                    //random size between 3 types
                    _newPanel.sizes.Add(Random.Range(0, 3));
                }
                //ADD PANEL TO LIST
                _newLevel.lvlPanelsList.Add(_newPanel);
                _newLevel.difficulty = difficulty;

                //create panel asset
                AssetDatabase.CreateAsset(_newPanel, "Assets/Resources/Levels/LevelList" + dif + "/Level" + (directoryLevels + lvls) + "/Panel" + panel + ".asset");
            }
           
            //create level asset
            AssetDatabase.CreateAsset(_newLevel, "Assets/Resources/Levels/LevelList" + dif + "/Level" + (directoryLevels + lvls) + ".asset");
          
            //ADD LEVELS TO LIST
            _lvlList._lvlList.Add(_newLevel);
        }
        
        // SORT OUR LEVEL LIST BY DIFFICULTY 
        _lvlList._lvlList.Sort((p1, p2) => p1.difficulty.CompareTo(p2.difficulty));

        if (!_levelsExist)
        {
            AssetDatabase.CreateAsset(_lvlList, "Assets/Resources/Levels/Levels.asset");
        }
        AssetDatabase.SaveAssets();
    }
#endif
}

