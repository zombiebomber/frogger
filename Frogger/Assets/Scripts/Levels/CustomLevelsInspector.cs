﻿using System;
using UnityEngine;
using UnityEditor;
using System.IO;


public enum DIFFICULTY
{
    Easy = 0,
    Medium =1,
    Hard =2
}
#if UNITY_EDITOR
[CustomEditor(typeof(LevelPanelDataGenerator))]
public class CustomLevelsInspector : Editor 
{
    static int LvlCount;
    LevelPanelDataGenerator lvlList;
    public DIFFICULTY dif;
    //LevelPanelDataGenerator _newLevels = new LevelPanelDataGenerator();

    private void OnEnable()
    {
        lvlList = (LevelPanelDataGenerator)target;
    }
    public override void OnInspectorGUI()
    {
        LvlCount = EditorGUILayout.IntField("Number of levels:", LvlCount);
        dif = (DIFFICULTY)EditorGUILayout.EnumPopup("Levels Difficulty: ", dif);

        if (GUILayout.Button("Create Levels"))
        {
            //create everything
            lvlList.DataGenerator(LvlCount, Convert.ToInt32(dif));
        }
        //    base.DrawDefaultInspector();

    }
}
#endif
