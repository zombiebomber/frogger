﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPanel : ScriptableObject
{
    public float speed;
    public bool type;
    public List<int> sizes =new List<int>();
}
