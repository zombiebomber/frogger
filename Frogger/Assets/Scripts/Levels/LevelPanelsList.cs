﻿using System.Collections.Generic;
using UnityEngine;

public class LevelPanelsList : ScriptableObject
{
    public int difficulty;
    public List<LevelPanel> lvlPanelsList = new List<LevelPanel>();
}
