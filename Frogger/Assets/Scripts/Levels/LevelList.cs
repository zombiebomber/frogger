﻿using System.Collections.Generic;
using UnityEngine;

public class LevelList : ScriptableObject
{
    public List<LevelPanelsList> _lvlList = new List<LevelPanelsList>();
}
