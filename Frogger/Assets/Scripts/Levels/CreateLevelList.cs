﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class CreateLevelList 
{
#if UNITY_EDITOR
    [MenuItem("Assets/Levels/CreateLevels")]
    public static LevelPanelDataGenerator Create()
    {
        LevelPanelDataGenerator asset= (LevelPanelDataGenerator)AssetDatabase.LoadAssetAtPath("Assets/LevelPanelDataGenerator.asset", typeof(LevelPanelDataGenerator));
        if (asset == null)
        {
            asset= ScriptableObject.CreateInstance<LevelPanelDataGenerator>();
        }

        if (!Directory.Exists("Assets/CreateLevels.asset"))
        {
            AssetDatabase.CreateAsset(asset, "Assets/CreateLevels.asset");
            AssetDatabase.SaveAssets();
        }
        return asset;
    }
#endif
}