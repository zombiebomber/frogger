﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour
{
    public static SplashScreen Instance;

    #region InspectorVars
    [SerializeField] GameObject _fadingObject;
    [SerializeField] Image _progressBar;
    [SerializeField] GameObject _tappingArea;
    #endregion

    #region UnityMethods
    private void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
        _progressBar.fillAmount -= Time.deltaTime;
    }
    #endregion
    #region PublicMethods
    public void StartFading()
    {
        _tappingArea.SetActive(false);
        _progressBar.fillAmount = 1;
        _fadingObject.SetActive(true);
        StartCoroutine(Fading());
    }
    #endregion
    IEnumerator Fading()
    {
        yield return new WaitForSeconds(3);
        _tappingArea.SetActive(true);
        _fadingObject.SetActive(false);
    }
}
