﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetPopUp : MonoBehaviour
{
    public static SetPopUp Instance;

    #region InspectorVars
    [SerializeField] Text _congrats;
    [SerializeField] Text _score;
    [SerializeField] GameObject _nextLevel;
    [SerializeField] GameObject _resetLevel;
    #endregion

    #region UnityMethods
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    #region PublicMethos
    public void SetValue(bool win)
    {
        if (win)
        {
            _congrats.text = "You Win!!!";
            _score.text = "Score: " + InGameUI.Instance.Score;
            _nextLevel.SetActive(true);
            _resetLevel.SetActive(false);
        }
        else
        {
            _congrats.text = "You Loose :(";
            _score.text = "Try Again!!!";
            _nextLevel.SetActive(false);
            _resetLevel.SetActive(true);
        }
    }
    #endregion
}
