﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapController : MonoBehaviour
{
    public static MapController Instance;

    #region InspectorVars

    [Header("Panels Section")]
    List<GameObject> _panelElements;
    [SerializeField] GameObject _panelsParent;
    [SerializeField] GameObject _streetPanel;
    [SerializeField] GameObject _riverPanel;
    [SerializeField] GameObject _grassPanel;

    [SerializeField] GameObject _startBox;
    [SerializeField] GameObject _winBox;

    [SerializeField] GameObject _spawnPointsParent;
    [SerializeField] Transform _spawnPoint;
    [Header("Levels List")]
    #endregion

    #region PrivateVars
    LevelList _lvlsList;
    #endregion

    #region Properties
    public GameObject RiverPanel { get { return _riverPanel; }set { _riverPanel = value; } }
    public  GameObject WinBox { get { return _winBox; } }
    #endregion


    #region UnityMethods
    private void Awake()
    {
        Instance = this;
        _lvlsList = GameManager.Instance.LevelList;
    }
    private void Start()
    {
        SetLvl();
    }
    #endregion

    #region PublicMethods
    public void SetLvl(int lvl=default(int))
    {
        if (lvl== null || lvl == 0)
        {
            lvl = PlayerPrefs.GetInt("CurrentLvl", 0);
        }
        LoadLvl(lvl);
    }
    public void NextLevel(int choosedLvl)
    {
        for (int i = _spawnPointsParent.transform.childCount - 1; i >= 0; i--)
        { 
            Destroy(_spawnPointsParent.transform.GetChild(i).gameObject);
        }
        for (int i = _panelsParent.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(_panelsParent.transform.GetChild(i).gameObject);
        }
        StartCoroutine(WaitToLoad(choosedLvl));
    }
    #endregion

    #region PrivateMethods
    void LoadLvl(int _lvl)
    {
        if (_lvl >=_lvlsList._lvlList.Count-1)
        {
            _lvl = 0;
            PlayerPrefs.SetInt("CurrentLvl", 0);
        }
        //mam liste leveli wybieram level mam liste paneli wybieram panel i tworze liste 
        //create that much lvl panels 
        float newpos = _startBox.transform.position.z + 2;
        bool grass = false;
        for (int i = 0; i < _lvlsList._lvlList[_lvl].lvlPanelsList.Count; i++)
        {
            // set position for new panel
            if(i>0)
            {
                newpos += 2;
            }
            //Create street and river panels then create for each lvl list of elements
            //if type is true we have streeto panel
            if (_lvlsList._lvlList[_lvl].lvlPanelsList[i].type)
            {
                Instantiate(_streetPanel, new Vector3(0, 0, newpos), Quaternion.identity).transform.SetParent(_panelsParent.transform);
            }
            //else we have wood panel
            else
            {
                if(!grass)
                {
                    Instantiate(_grassPanel, new Vector3(0, 0, newpos), Quaternion.identity).transform.SetParent(_panelsParent.transform);
                    newpos = newpos + 2;
                    grass = true;
                }
                Instantiate(_riverPanel, new Vector3(0, 0, newpos), Quaternion.identity).transform.SetParent(_panelsParent.transform);
            }

            int direction = 1;
            //CREATE NEW SPAWNPOINTS FOR PANEL AND DIRECTION
            Instantiate(_spawnPoint).transform.SetParent(_spawnPointsParent.transform);
            if (i % 2 == 0)
            {
                direction = -1;
                _spawnPointsParent.GetComponentsInChildren<SpawningScript>()[i].transform.position = new Vector3(-_spawnPoint.transform.position.x, _spawnPoint.transform.position.y, newpos);
            }
            else
            {
                _spawnPointsParent.GetComponentsInChildren<SpawningScript>()[i].transform.position = new Vector3(_spawnPoint.transform.position.x, _spawnPoint.transform.position.y, newpos);
            }
            //set spawn point values
            _spawnPointsParent.GetComponentsInChildren<SpawningScript>()[i].LvlNumber = _lvl;
            _spawnPointsParent.GetComponentsInChildren<SpawningScript>()[i].PanelNumber = i;
            _spawnPointsParent.GetComponentsInChildren<SpawningScript>()[i].Direction = direction;
            _spawnPointsParent.GetComponentsInChildren<SpawningScript>()[i].PanelType = _lvlsList._lvlList[_lvl].lvlPanelsList[i].type;
        }
        //set win box
        Instantiate(_winBox, new Vector3(0, 0, newpos+2), Quaternion.identity).transform.SetParent(_panelsParent.transform);
       
        //set timer value 
        InGameUI.Instance.Time = Convert.ToInt32(Math.Pow(Convert.ToDouble(_lvlsList._lvlList[_lvl].lvlPanelsList.Count), Convert.ToDouble(3)));
        InGameUI.Instance.SetLevel();
    }
    IEnumerator WaitToLoad(int choosedLvl)
    {
        yield return new WaitForSeconds(1);
        SetLvl(choosedLvl);
    }
    #endregion
}