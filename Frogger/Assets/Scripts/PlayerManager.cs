﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager Instance;

    #region InspectorVars
    [SerializeField] Camera _camera;
    [SerializeField] GameObject _Player;
    [SerializeField] Rigidbody _rb;

    [Header("Tapping Areas")]
    [SerializeField] Button _up;
    [SerializeField] Button _down;
    [SerializeField] Button _left;
    [SerializeField] Button _rigth;
    #endregion

    #region PrivateVars
    bool _isGrounded =true;
    float _faling = 3F;
    float _jumpLength = 3F;
    #endregion
   
    #region PublicVars
   public float MaxHeigth = 0;
   public float JumpHeigth = 5.5F;
    #endregion

    #region Properties
    public bool IsGrounded { set { _isGrounded = value; } }
    #endregion

    #region UnityMethods
    private void Awake()
    {
        Instance = this;
        _up.onClick.AddListener(delegate { ButtonUp(); });
        _down.onClick.AddListener(delegate { ButtonDown(); });
        _left.onClick.AddListener(delegate { ButtonLeft(); });
        _rigth.onClick.AddListener(delegate { ButtonRigth(); });
    }

    void Update()
    {
#if UNITY_EDITOR
        //CHECK THAT WE CLICKED SOMETHING
        if (Input.GetKeyDown(KeyCode.RightArrow)  )
            {
                ButtonRigth();
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                ButtonLeft();
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                ButtonUp();
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                ButtonDown();
            }
#endif
        //wherewer we move out player we only need to change our height
        _rb.velocity += Vector3.up * Physics2D.gravity.y * (_faling - 1) * Time.deltaTime;
    }
    #endregion

    #region PublicMethods
    public void ButtonUp()
    {
        if (_isGrounded)
        {
            _rb.GetComponent<Rigidbody>().AddForce(new Vector3(0, JumpHeigth, _jumpLength), ForceMode.Impulse);
            _isGrounded = false;
        }
    }
    public void ButtonDown()
    {
        if (_isGrounded)
        {
            _rb.GetComponent<Rigidbody>().AddForce(new Vector3(0, JumpHeigth, -_jumpLength), ForceMode.Impulse);
            _isGrounded = false;
        }
    }
    public void ButtonLeft()
    {
       if (_isGrounded)
        {
            _rb.GetComponent<Rigidbody>().AddForce(new Vector3(-_jumpLength, JumpHeigth, 0), ForceMode.Impulse);
            _isGrounded = false;
        }
    }
    public void ButtonRigth()
    {
        if (_isGrounded)
        {
            _rb.GetComponent<Rigidbody>().AddForce(new Vector3(_jumpLength, JumpHeigth, 0), ForceMode.Impulse);
            _isGrounded = false;
        }
    }
  
    public void ResetPlayer()
    {
        _Player.transform.position = new Vector3(0,0,0);
    }
    #endregion
}
