﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadGame : MonoBehaviour
{
    [SerializeField] Image _progressBar;

    private void Start()
    {
        StartCoroutine(LoadAsyncOperation());
        SceneManager.LoadScene(1);
    }
    IEnumerator LoadAsyncOperation()
    {
        AsyncOperation loadScene = SceneManager.LoadSceneAsync(1);
        while(loadScene.progress<1)
        {
            _progressBar.fillAmount -= loadScene.progress;
            yield return new WaitForEndOfFrame();
        }
    }

}
