﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    public static InGameUI Instance;

    #region InspectorVars
    [SerializeField] GameObject _lifes;
    [SerializeField] GameObject _lifesParent;
    [SerializeField] GameObject _tryes;
    [SerializeField] GameObject _tryesParent;
    [SerializeField] Text _timer;
    [SerializeField] GameObject _pauseMenu;
    #endregion

    #region PrivateVars
    int _score = 0;
    int _panelsCount = 0;
    int _time = 0;
    Coroutine _timerCoroutine =null;
    #endregion

    #region Properties
    public int PanelsCount { set { _panelsCount = value; } }
    public int Time { set { _time = value; } }
    public int Score { get { return _score; } }
    #endregion

    #region UnityMethods
    private void Awake()
    {
        Instance = this;
    }
    #endregion
    #region PublicMethods
    public void SetTime()
    {
        _timer.text = "Time: " + _time + "s";
        if(_timerCoroutine != null)
        {
            StopCoroutine(_timerCoroutine);
        }
            _timerCoroutine = StartCoroutine(Timer(_time));
    }
    public void SetLevel()
    {
        while (_lifesParent.GetComponentsInChildren<Image>().Length<3)
        {
            Instantiate(_lifes, _lifesParent.transform);
        }
        for (int i = _tryesParent.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(_tryesParent.transform.GetChild(i).gameObject);
        }
        SetTime();
        _score = 0;
    }
    public void Die()
    {
        Destroy(_lifesParent.GetComponentsInChildren<Image>()[_lifesParent.GetComponentsInChildren<Image>().Length - 1].gameObject);

        if (_lifesParent.GetComponentsInChildren<Image>().Length == 1)
        {
            GameManager.Instance.Loose();
        }
        else
        {
            GameManager.Instance.ResetPlayer();
            SetTime();
        }
    }
    public void CompleteRun()
    {
        Instantiate(_tryes, _tryesParent.transform);
        int lvl  = PlayerPrefs.GetInt("CurrentLvl", 0);
        string points = Regex.Match(_timer.text,@"\d+").Value;
        _score += Convert.ToInt32(points) * GameManager.Instance.LevelList._lvlList[lvl].lvlPanelsList.Count * _panelsCount;

        if (_tryesParent.GetComponentsInChildren<Image>().Length == 3)
        {
            GameManager.Instance.Win(_score);
        }
        else
        {
            GameManager.Instance.ResetPlayer();
        }
    }
    public void GamePause()
    {
        UnityEngine.Time.timeScale = 0f;
        _pauseMenu.SetActive(true);
    }
    public void GameUnPause()
    {
        _pauseMenu.SetActive(false);
        UnityEngine.Time.timeScale = 1f;
    }
    public void ResetLevel()
    {
        _pauseMenu.SetActive(false);
        GameManager.Instance.ResetLevel();
    }
    public void OpenMenu()
    {
        _pauseMenu.SetActive(false);
        GameManager.Instance.OpenMenu();
    }
    #endregion

    IEnumerator Timer(int time)
    {
        while (time >0)
        {
            time--;
            _timer.text = "Time: " + time + "s";
            yield return new WaitForSeconds(1);
        }
        Die();
       // yield return null;
    }
}
