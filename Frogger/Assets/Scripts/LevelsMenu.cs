﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelsMenu : MonoBehaviour
{
    public static LevelsMenu Instance;

    #region InspectorVars
    [SerializeField] GameObject _levelsMenu;
    [SerializeField] Button _level;
    [SerializeField] List<GameObject> _difficultyPanels;
    #endregion

    #region UnityMethods
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    #region PublicMethos
    public void ShowMenu()
    {
        _levelsMenu.SetActive(true);
    }
    public void HideMenu()
    {
        _levelsMenu.SetActive(false);
    }
    public void ShowMainMenu()
    {
        _levelsMenu.SetActive(false);
        GameManager.Instance.OpenMenu();
    }
    #endregion
    //filling opened panel with data 
    public void FillMenu(int difficulty)
    {
        //why? because when we complete level we need change his status to active so just reload list
        for (int i = _difficultyPanels[difficulty].transform.childCount-1; i>=0;i--)
        {
            Destroy(_difficultyPanels[difficulty].transform.GetChild(i).gameObject);
        }
        int _currentLvl = PlayerPrefs.GetInt("CurrentLvl", 0);
        for(int i =0; i < GameManager.Instance.LevelList._lvlList.Count; i++)
        {
            if (GameManager.Instance.LevelList._lvlList[i].difficulty == difficulty)
            {
                Button nextBtn =  Instantiate(_level, _difficultyPanels[difficulty].transform);
                nextBtn.GetComponentInChildren<Text>().text = i.ToString();
                if(i<= _currentLvl)
                {
                    nextBtn.onClick.AddListener(delegate { GameManager.Instance.NextLevel(i); HideMenu(); });
                }
                else
                {
                    nextBtn.interactable = false;
                }
            }
        }
    }
}
