﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpeed : MonoBehaviour
{
    #region PrivateVars
    bool _direction = false;
    #endregion

    #region PublicVars
    public float speed;
    public Rigidbody _obstacleRb;
    #endregion

    #region Properties
    public bool Direction { set { _direction = value; } }
    #endregion


    #region PrivateVars
    private void FixedUpdate()
    {
        //destroy if its out of view
        if(_direction)
        {
            if(_obstacleRb.position.x >= 14)
            {
                Destroy(_obstacleRb.gameObject);
            }
        }else
        {
            if (_obstacleRb.position.x <= -14)
            {
                Destroy(_obstacleRb.gameObject);
            }
        }
        //new position
         Vector3 move = new Vector3(transform.right.x+100, transform.right.y, transform.right.z);
        _obstacleRb.MovePosition(_obstacleRb.position + move * Time.fixedDeltaTime * speed);
    }
    #endregion
}
