﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningScript : MonoBehaviour
{
    public static SpawningScript Instance;

    #region InspectorVars
    [SerializeField] List<GameObject> _carsTypes;
    [SerializeField] List<GameObject> _woodsTypes;
    [SerializeField] Transform _spTransform;
    #endregion

    #region PrivateVars
    int[] _distance = { 2, 3, 4 };
    int _panelNumber;
    int _lvlNumber;
    bool _panelType;
    int _direction = 1;
    float spawnDelay = 10;
    float nextTimeToSpawn = 0F;
    int el = 0;
    float _speed;

    LevelList _levelList;
    #endregion

    #region Properties
    public int PanelNumber { set { _panelNumber = value; } }
    public int LvlNumber { set { _lvlNumber = value; } }
    public bool PanelType { set { _panelType = value; } }
    public int Direction { set { _direction = value; } }
    #endregion

    #region UnityMethods
    private void Start()
    {
        Instance = this;
        _levelList = GameManager.Instance.LevelList;
        _speed = _levelList._lvlList[_lvlNumber].lvlPanelsList[_panelNumber].speed;
    }
    private void Update()
    {
        //if we spawn all list then we reset 
        if (el < _levelList._lvlList[_lvlNumber].lvlPanelsList[_panelNumber].sizes.Count)
        {
            //Spawn our panels elements
            if (nextTimeToSpawn <= Time.time)
            {
                //check panel type
                if (_panelType)
                {
                    GameObject _newCar = Instantiate(_carsTypes[_levelList._lvlList[_lvlNumber].lvlPanelsList[_panelNumber].sizes[el]], _spTransform);
                    _newCar.GetComponent<ObstacleSpeed>().speed = _speed * _direction;

                    //set object direction and rotation
                    if (_direction>0)
                    {
                        _newCar.GetComponent<ObstacleSpeed>().Direction = true;
                        _newCar.transform.rotation =new Quaternion(_newCar.transform.rotation.x, 180, _newCar.transform.rotation.z, _newCar.transform.rotation.w);
                    }
                    else
                    {
                        _newCar.GetComponent<ObstacleSpeed>().Direction = false;
                    }
                }
                else
                {
                    GameObject _newWood = Instantiate(_woodsTypes[_levelList._lvlList[_lvlNumber].lvlPanelsList[_panelNumber].sizes[el]], _spTransform);
                    _newWood.GetComponent<ObstacleSpeed>().speed = _speed * _direction;
                    //set object direction and rotation
                    if (_direction > 0){_newWood.GetComponent<ObstacleSpeed>().Direction = true;}else{_newWood.GetComponent<ObstacleSpeed>().Direction = false;}
                    // lucky lucky  you diving log
                    int diver = 0;
                    diver=  Random.Range(0, 3);
                    if(diver == 0)
                    {
                        //int randomDiver = Random.Range(0, _spTransform.GetComponentsInChildren<BoxCollider>().Length - 1);
                        //run animation
                        _spTransform.GetComponentsInChildren<BoxCollider>()[el].GetComponentInChildren<Animation>().enabled = true;
                    }
                }
                //take element before interation
                int previousElement = _distance[_levelList._lvlList[_lvlNumber].lvlPanelsList[_panelNumber].sizes[el]];

                //iteration for spawning
                el++;

                //define next spawn time depends from next element size 
                int nextEl;
                int multiplier = 1;

                //get sizes
                if (el == 3)
                {
                    nextEl = 0;
                    nextEl = _distance[_levelList._lvlList[_lvlNumber].lvlPanelsList[_panelNumber].sizes[nextEl]];
                }
                else
                {
                    nextEl = _distance[_levelList._lvlList[_lvlNumber].lvlPanelsList[_panelNumber].sizes[el]];
                }
                
                if (previousElement < nextEl)
                {
                    multiplier = 2;
                }
                //time for spawn is a first element speed for his distance + next element half size distance speed value 
                //probably it is ...
                spawnDelay = previousElement / (_speed*100) + ( nextEl / (_speed * 100)*multiplier); //(_speed * 1000) * Mathf.Pow(_distance[nextEl],2)/2;
                nextTimeToSpawn = Time.time + spawnDelay;
            }
        }
        else
        {
            el = 0;
        }
    }
    #endregion
}



