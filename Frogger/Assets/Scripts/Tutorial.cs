﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    #region InspectorVars
    [SerializeField] GameObject _tutorial;
    [SerializeField] GameObject _tappingArea;
    #endregion

    #region UnityMethods
    public void Hide()
    {
        PlayerPrefs.SetInt("IsFirstGame", 1);
        _tutorial.SetActive(false);
        _tappingArea.SetActive(true);
    }
    #endregion
}
